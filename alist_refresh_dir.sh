#!/bin/bash

host="http://127.0.0.1:5244"
username="admin"
password="261018"
# root_path="/xxx_pan/crypt"
# rclone_path="alist:/xxx_pan/crypt"
root_path="$1"
rclone_path="alist:"$1""

token=$(
    curl --location --request POST ''$host'/api/auth/login' \
    --header 'Content-Type: application/json' \
    --header 'Connection: keep-alive' \
    --data-raw '{
        "username": "'$username'",
        "password": "'$password'"
    }' | awk -F"token\":\"" '{print $2}' | awk -F"\"}}" '{print $1}'
)


rclone lsd "$rclone_path" -R | awk '{print $(NF)}' | xargs -I {} \
curl --location --request POST ''$host'/api/fs/list' \
--header 'Authorization: '$token'' \
--header 'Content-Type: application/json' \
--data-raw '{
    "path": "'$root_path'/{}",
    "page": 1,
    "per_page": 0,
    "refresh": true
}'
