#!/bin/bash

sudo sed -i '/Exec/ s/$/ --proxy-server="http:\/\/127.0.0.1:7890"/g' /usr/share/applications/google-chrome.desktop

echo "Chrome proxy settings is ready"
