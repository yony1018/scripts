## Cloudflare DDNS config
```
cd /root
wget https://gitlab.com/yony1018/scripts/-/raw/main/CloudflareConfig/CF_DDNS.sh
chmod 777 ./CF_DDNS.sh
```
`crontab -e`
```
*/5 * * * * /root/CF_DDNS.sh -r <domain_name> -t <A_or_AAAA> > /dev/null 2>&1
```

## ddns: Cloudflare & selector: better_CFip
```
mkdir BTCF_CFddns
cd BTCF_CFddns
curl --output CF_DDNS.sh https://gitlab.com/yony1018/scripts/-/raw/main/CloudflareConfig/BTCF_CFddns/CF_DDNS.sh
curl --output Cloudflare_DDNS.sh https://gitlab.com/yony1018/scripts/-/raw/main/CloudflareConfig/BTCF_CFddns/Cloudflare_DDNS.sh
curl --output cf.sh https://gitlab.com/yony1018/scripts/-/raw/main/CloudflareConfig/BTCF_CFddns/cf.sh

curl --output cfst.tar.gz https://gitlab.com/yony1018/scripts/-/raw/main/CloudflareConfig/CloudflareST_linux_amd64.tar.gz
tar -zxvf cfst.tar.gz
rm cfst.tar.gz
chmod 777 ./ -R
```

`crontab -e`

```
0 */1 * * * /root/BTCF_CFddns/Cloudflare_DDNS.sh -r select.yonyvp2.cf -v 4 -s 40 > /dev/null 2>&1
0 */1 * * * /root/BTCF_CFddns/Cloudflare_DDNS.sh -r select.yonyvp2.cf -v 6 -s 40 > /dev/null 2>&1
0 */1 * * * /root/BTCF_CFddns/Cloudflare_DDNS.sh -r select6.yonyvp2.cf -v 6 -s 40 > /dev/null 2>&1
```

## ddns: Cloudflare & selector: CloudflareSpeedTest
```
mkdir CFST_CFddns
cd CFST_CFddns
curl --output CF_DDNS.sh https://gitlab.com/yony1018/scripts/-/raw/main/CloudflareConfig/CFST_CFddns/CF_DDNS.sh
curl --output CFip_SelectUpdate.sh https://gitlab.com/yony1018/scripts/-/raw/main/CloudflareConfig/CFST_CFddns/CFip_SelectUpdate.sh

curl --output cfst.tar.gz https://gitlab.com/yony1018/scripts/-/raw/main/CloudflareConfig/CloudflareST_linux_amd64.tar.gz
tar -zxvf cfst.tar.gz
rm cfst.tar.gz
chmod 777 ./ -R
```

`crontab -e`

```
0 0,12 * * * /root/CFST_CFddns/CFip_SelectUpdate.sh -r select.yonyvp2.cf -v 4 -s 10 > /dev/null 2>&1
0 0,12 * * * /root/CFST_CFddns/CFip_SelectUpdate.sh -r select.yonyvp2.cf -v 6 -s 5 > /dev/null 2>&1
0 0,12 * * * /root/CFST_CFddns/CFip_SelectUpdate.sh -r select6.yonyvp2.cf -v 6 -s 5 > /dev/null 2>&1
```

## ddns: NS1 & selector: CloudflareSpeedTest

```
cd /root
mkdir CFST_NS1ddns
cd CFST_NS1ddns
curl --output CFip_NS1_update.sh https://gitlab.com/yony1018/scripts/-/raw/main/CloudflareConfig/CFST_NS1ddns/CFip_NS1_update.sh

curl --output cfst.tar.gz https://gitlab.com/yony1018/scripts/-/raw/main/CloudflareConfig/CloudflareST_linux_amd64.tar.gz
tar -zxvf cfst.tar.gz
rm cfst.tar.gz
chmod 777 ./ -R
```

`crontab -e`

```
0 */1 * * * /root/CFST_NS1ddns/CFip_NS1_update.sh -v 4 -s 4 > /dev/null 2>&1
0 */1 * * * /root/CFST_NS1ddns/CFip_NS1_update.sh -v 6 -s 4 > /dev/null 2>&1
0 */1 * * * /root/CFST_NS1ddns/CFip_NS1_update.sh -v 6 -s 4 -r select.yonyvp.cf > /dev/null 2>&1
```