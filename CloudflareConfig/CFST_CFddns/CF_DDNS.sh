#!/bin/bash

bearer="6PlICVk8KhcbQwnWs-IK2TLMaxn0r7iLwlfe_TWx"
zone_name=""    #mid_parameter
zone_id=""      #mid_parameter
record_name=""
record_type=""
ipaddr=""
ttl="120"      #opt
proxied="false"      #opt

v6addr=$(ip addr | grep "inet6" | grep "global dynamic" | awk -F '/' '{print$1}' | awk '{print$2}')
v4addr=$(ip addr | grep "inet" | grep "global" | grep "ppp0" | awk -F '/' '{print$1}' | awk '{print$2}')



while getopts ":b:r:t:i:l:p:h:" flag
do
    case "${flag}" in
        b)  
            echo "Using bearer: ${OPTARG}";
            bearer=${OPTARG};;
        r) 
            echo "Using record name: ${OPTARG}";
            record_name=${OPTARG};;
        t)
            echo "Using record type: <${OPTARG}>";
            record_type=${OPTARG};;
        i)
            echo "Using ip addr: ${OPTARG}";
            ipaddr=${OPTARG};;
        l)
            echo "Using modified ttl: ${OPTARG}";
            ttl=${OPTARG};;
        p)
            echo "Using modified proxied: ${OPTARG}"
            proxied=${OPTARG};;
        h)
            echo "Usage: -b <bearer> -r <record_name> -t <record_type> -i <ip_addr>";
            echo "Optional: -l <ttl> -p <proxied_true_or_false>";
            exit 1;;
    esac
done


if [ -z $bearer ]
then echo "NO BEARER received, CHECK -b <bearer> as input!"; 
exit 1;
fi

if [ -z $record_name ]
then echo "NO RECORD NAME received, CHECK -r <record_name> as input!"; 
exit 1;
fi

if [ -z $record_type ]
then echo "NO RECORD TYPE received, CHECK -t <record_type> as input!"; 
exit 1;
fi

if [ -z $ipaddr ]
then 
    echo "NO IP ADDRESS received, CHECK -i <ip_addr> as input! OR update ip with AUTO-GRUBED";
    if [ "$record_type" == "AAAA" ]
    then 
        ipaddr=$v6addr;
        echo "update ipv6 addr: $ipaddr";
    elif [ "$record_type" == "A" ]
    then 
        ipaddr=$v4addr;
        echo "update ipv4 addr: $ipaddr";
    else echo "Unknown ip type!";
    fi
    if [ -z $ipaddr ]
    then echo "IP ADDRESS INPUT STILL EMPTY! EXIT!"
    exit 1;
    fi
fi




zone_name=$(echo $record_name | awk -F '.' '{print$(NF-1) "." $NF}')
zone_id=$(curl -k -X GET "https://api.cloudflare.com/client/v4/zones" \
-H "Authorization: Bearer $bearer" \
-H "Content-Type:application/json" \
| awk -F $zone_name '{print$1}' | awk -F '\"id\":\"' '{print$NF}' | awk -F '\"' '{print$1}')

echo "Using $zone_name as zone name, zone id is $zone_id"

split=$record_name\",\"type\":\"$record_type
record_id=$(curl -k -X GET "https://api.cloudflare.com/client/v4/zones/$zone_id/dns_records" \
-H "Content-Type:application/json" \
-H "Authorization: Bearer $bearer" \
| awk -F $split '{print$1}' | awk -F '\"id\":\"' '{print$NF}' | awk -F '\"' '{print$1}')

echo "Return record id as: $record_id"



curl -k -X PUT "https://api.cloudflare.com/client/v4/zones/$zone_id/dns_records/$record_id" \
-H "Content-Type: application/json" \
-H "Authorization: Bearer $bearer" \
--data '{"type":"'$record_type'","name":"'$record_name'","content":"'$ipaddr'","ttl":'$ttl',"proxied":'$proxied'}'


echo "parameter checklist: $bearer $zone_name $zone_name_chk $zone_id 
$record_id"


