#!/bin/bash

basepath=$(cd `dirname $0`; pwd)
cd $basepath

bearer=""
ip_version=""
record_name=""
min_speed=0

while getopts ":b:v:r:s:h:" flag
do
    case "${flag}" in
        b)  
            echo "Using bearer: ${OPTARG}";
            bearer=${OPTARG};;
        v)  
            echo "Using ip version: IPv${OPTARG}";
            ip_version=${OPTARG};;
        r) 
            echo "Using record name: ${OPTARG}";
            record_name=${OPTARG};;
        s)
            echo "Setting ${OPTARG}/MB as minimum speed";
            min_speed=${OPTARG};;
        h)
            echo "Usage: -v <ip_version_4or6> -r <record_name>";
            echo "Optional: -s <min_speed>";
            exit 1;;
    esac
done


if [ -z $record_name ]
then echo "NO RECORD NAME received, CHECK -r <record_name> as input!"; exit 1;
fi


if [ -z $ip_version ]; then 
    echo "NO IP VERSION received, CHECK -v <ip_version> as input!"; exit 1;
elif [ $ip_version -eq 4 ]; then
    ./CloudflareST -url https://cdn.cloudflare.steamstatic.com/steam/apps/256843155/movie_max.mp4 -f ./ip.txt -o ./result.csv
    v4spd=$(cat ./result.csv | head -n 2 | tail -n 1 | awk -F ',' '{print$6}' | awk -F '.' '{print$1}');
    v4addr=$(cat ./result.csv | head -n 2 | tail -n 1 | awk -F ',' '{print$1}');
    if [ $v4spd -ge $min_speed ]; then
        echo "Update CF selected v4ip as: $v4addr, speed at $v4spd"
        ./CF_DDNS.sh -r $record_name -t "A" -i $v4addr -l 60
    else
        echo "STOP UPDATE cus: IPv4 speed list lower than $min_speed MB/s"
    fi
elif [ $ip_version -eq 6 ]; then
    ./CloudflareST -url https://cdn.cloudflare.steamstatic.com/steam/apps/256843155/movie_max.mp4 -f ./ipv6.txt -o ./resultv6.csv
    v6spd=$(cat ./resultv6.csv | head -n 2 | tail -n 1 | awk -F ',' '{print$6}' | awk -F '.' '{print$1}')
    v6addr=$(cat ./resultv6.csv | head -n 2 | tail -n 1 | awk -F ',' '{print$1}')
    if [ $v6spd -ge $min_speed ]; then
        echo "Update CF selected v6ip as: $v6addr, speed at $v6spd"
        ./CF_DDNS.sh -r $record_name -t "AAAA" -i $v6addr -l 60
    else
        echo "STOP UPDATE cus: IPv6 speed list lower than $min_speed MB/s"
    fi
else
    echo "Input wrong ip version, should be 4 or 6!"; exit 1;
fi
