#!/bin/bash

basepath=$(cd `dirname $0`; pwd)
cd $basepath

bearer=""
ip_version=""
record_name=""
#speed (Mbps)
min_speed=40

while getopts ":b:v:r:s:h:" flag
do
    case "${flag}" in
        b)  
            echo "Using bearer: ${OPTARG}";
            bearer=${OPTARG};;
        v)  
            echo "Using ip version: IPv${OPTARG}";
            ip_version=${OPTARG};;
        r) 
            echo "Using record name: ${OPTARG}";
            record_name=${OPTARG};;
        s)
            echo "Setting ${OPTARG}/Mbps as minimum speed";
            min_speed=${OPTARG};;
        h)
            echo "Usage: -v <ip_version_4or6> -r <record_name>";
            echo "Optional: -s <min_speed>";
            exit 1;;
    esac
done


if [ -z $record_name ]
then echo "NO RECORD NAME received, CHECK -r <record_name> as input!"; exit 1;
fi


if [ -z $ip_version ]; then 
    echo "NO IP VERSION received, CHECK -v <ip_version> as input!"; exit 1;
elif [ $ip_version -eq 4 ]; then
    ./cf.sh $min_speed 1 > result.txt
    v4spd=$(cat ./result.txt | grep "实测带宽" | awk -F ' ' '{print$2}');
    v4addr=$(cat ./result.txt | grep "优选IP" | awk -F ' ' '{print$2}');
    echo "Update CF selected v4ip as: $v4addr, speed at $v4spd"
    ./CF_DDNS.sh -r $record_name -t "A" -i $v4addr -l 60
elif [ $ip_version -eq 6 ]; then
    ./cf.sh $min_speed 3 > resultv6.txt
    v6spd=$(cat ./resultv6.txt | grep "实测带宽" | awk -F ' ' '{print$2}')
    v6addr=$(cat ./resultv6.txt | grep "优选IP" | awk -F ' ' '{print$2}')
    echo "Update CF selected v6ip as: $v6addr, speed at $v6spd"
    ./CF_DDNS.sh -r $record_name -t "AAAA" -i $v6addr -l 60
else
    echo "Input wrong ip version, should be 4 or 6!"; exit 1;
fi
