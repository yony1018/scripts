#!/bin/bash

#------------------------args------------------------#
Account_Email="yony1018@foxmail.com"
Global_API_Key="e88cb4e036e37d9cab0f6b1cc7414ad893e37"

record_name=""
record_type=""

ipaddr=""             #opt
is_proxied="false"    #opt
ttl="120"               #opt, ttl 1 means auto=5min

zone_name=""          #mid_parameter
zone_id=""            #mid_parameter

#------------------------Process IP stuff------------------------#
hosts=("checkip.amazonaws.com" "api.ipify.org" "ifconfig.me/ip" "icanhazip.com" "ipinfo.io/ip" "ipecho.net/plain" "checkipv4.dedyn.io")
CURL=`which curl`
DIG=`which dig`
# useless, sometime error:
# check=$($DIG +short myip.opendns.com @resolver1.opendns.com A)  
if [ ! $? -eq 0 ] || [ -z "$check" ] || [[ ! $check =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
    echo "Unable to get your public IP address by OpenDNS service, try to another way."
    count=${#hosts[@]}
    while [ -z "$check" ] && [[ $count -ne 0 ]]; do
        selectedhost=${hosts[ $RANDOM % ${#hosts[@]} ]}
        check=$($CURL -4s https://$selectedhost | grep '[^[:blank:]]') && {
            if [ -n "$check" ] && [[ $check =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
                break
            else
                check=""
                count=$(expr $count - 1)
                echo "The host $selectedhost returned an invalid IP address."
            fi
        } || {
            check=""
            count=$(expr $count - 1)
            echo "The host $selectedhost did not respond."
        }
    done
fi
if [ -z "$check" ]; then
    echo "Unable to get your public IP address. Please check your internet connection."
fi
echo "Your public IP address is $check"


v6addr=$(ip addr | grep "inet6" | grep "global dynamic" | awk -F '/' '{print$1}' | awk '{print$2}')
v4addr=$(ip addr | grep "inet" | grep "global" | grep "ppp0" | awk -F '/' '{print$1}' | awk '{print$2}')

if [ "$check" == "$v4addr" ]; then
    echo "Public ip is EQUAL to local ip! That's OK!"
    else
    echo "Public ip is NOT EQUAL to local ip! Use public ip as default!"
    v4addr=$check
fi

while getopts ":e:a:r:t:i:l:p:h:" flag
do
    case "${flag}" in
        e)  
            echo "Using Account Email: ${OPTARG}";
            Account_Email=${OPTARG};;
        a)  
            echo "Using Global API Key: ${OPTARG}";
            Global_API_Key=${OPTARG};;
        r) 
            echo "Using record name: ${OPTARG}";
            record_name=${OPTARG};;
        t)
            echo "Using record type: <${OPTARG}>";
            record_type=${OPTARG};;
        i)
            echo "Using ip addr: ${OPTARG}";
            ipaddr=${OPTARG};;
        l)
            echo "Using modified ttl: ${OPTARG}";
            ttl=${OPTARG};;
        p)
            echo "Using modified proxied: ${OPTARG}"
            is_proxied=${OPTARG};;
        h)
            echo "Usage: -e <Account_Email> -a <Global_API_Key> -r <record_name> -t <record_type> -i <ip_addr>";
            echo "Optional: -l <ttl> -p <is_proxied>";
            exit 1;;
    esac
done


if [ -z $Account_Email ]
then echo "NO Account_Email received, CHECK -e <Account_Email> as input!"; 
exit 1;
fi

if [ -z $Global_API_Key ]
then echo "NO Global_API_Key received, CHECK -a <Global_API_Key> as input!"; 
exit 1;
fi

if [ -z $record_name ]
then echo "NO RECORD NAME received, CHECK -r <record_name> as input!"; 
exit 1;
fi

if [ -z $record_type ]
then echo "NO RECORD TYPE received, CHECK -t <record_type> as input!"; 
exit 1;
fi

if [ -z $ipaddr ]
then 
    echo "NO IP ADDRESS received, CHECK -i <ip_addr> as input! OR update ip with AUTO-GRUBED";
    if [ "$record_type" == "AAAA" ]
    then 
        ipaddr=$v6addr;
        echo "update ipv6 addr: $ipaddr";
    elif [ "$record_type" == "A" ]
    then 
        ipaddr=$v4addr;
        echo "update ipv4 addr: $ipaddr";
    else echo "Unknown ip type!";
    fi
    if [ -z $ipaddr ]
    then echo "IP ADDRESS INPUT STILL EMPTY! EXIT!"
    exit 1;
    fi
fi

#------------------------Get zone_id------------------------#
zone_name=$(echo $record_name | awk -F '.' '{print$(NF-1) "." $NF}')
zone_id=$(
  curl -s -k -X GET \
    --url https://api.cloudflare.com/client/v4/zones \
    --header 'Content-Type: application/json' \
    --header 'X-Auth-Email: '$Account_Email'' \
    --header 'X-Auth-Key: '$Global_API_Key'' \
    | awk -F $zone_name '{print$1}' | awk -F '\"id\":\"' '{print$NF}' | awk -F '\"' '{print$1}'
)

echo "Using $zone_name as zone name, zone id is $zone_id"

#------------------------Get record_id------------------------#
split=$record_name\",\"type\":\"$record_type
record_id=$(
  curl -s -k -X GET \
    --url https://api.cloudflare.com/client/v4/zones/$zone_id/dns_records \
    --header 'Content-Type: application/json' \
    --header 'X-Auth-Email: '$Account_Email'' \
    --header 'X-Auth-Key: '$Global_API_Key'' \
    | grep $record_name | awk -F $split '{print$1}' | awk -F '\"id\":\"' '{print$NF}' | awk -F '\"' '{print$1}'
)

echo "Return record id as: $record_id"

if [ -z $record_id ]
then echo "ERROR on get RECORD ID: record name $record_name DOESN'T exist! Create one first!"
exit 1;
fi

#------------------------Update record------------------------#
is_successed=$(
  curl -s -k -X PATCH \
    --url https://api.cloudflare.com/client/v4/zones/$zone_id/dns_records/$record_id \
    --header 'Content-Type: application/json' \
    --header 'X-Auth-Email: '$Account_Email'' \
    --header 'X-Auth-Key: '$Global_API_Key'' \
    --data '{
    "proxied": '$is_proxied',
    "settings": {},
    "tags": [],
    "ttl": '$ttl',
    "content": "'$ipaddr'",
    "type": "'$record_type'"
  }' | grep "\"success\":true"
)

if [ -z $is_successed ]
then echo "ERROR ON UPDATE DNS! CHECK DETAIL INFO!"
exit 1;
else echo "UPDATE SUCCESSED!";
fi

