#!/bin/bash

# 指定测速地址；延迟测速(HTTPing)/下载测速时使用的地址，默认地址不保证可用性，建议自建
url1="https://cdn.cloudflare.steamstatic.com/steam/apps/256870924/movie_max.mp4"
url2="https://cdn.cloudflare.steamstatic.com/steam/apps/256843155/movie_max.mp4"
url3="https://download.parallels.com/desktop/v15/15.1.5-47309/ParallelsDesktop-15.1.5-47309.dmg"
url4="https://speed.cloudflare.com/__down?bytes=200000000"
url=$url1
# 下载速度下限；只输出高于指定下载速度的 IP，凑够指定数量 [-dn] 才会停止测速；(默认 0.00 MB/s)
min_speed=4
# 平均延迟上限；只输出低于指定平均延迟的 IP，各上下限条件可搭配使用；(默认 9999 ms)
max_relay=250

key="fu8vUVOWETQmTncvTowU"
record_name="select.yonyvp.cf"
record6_name="select6.yonyvp.cf"


while getopts ":k:v:r:s:h:" flag
do
    case "${flag}" in
        k)  
            echo "Using NS1_API_KEY: ${OPTARG}";
            key=${OPTARG};;
        v)  
            echo "Using ip version: IPv${OPTARG}";
            ip_version=${OPTARG};;
        r) 
            echo "Using record name: ${OPTARG}";
            rn=${OPTARG};;
        s)
            echo "Setting ${OPTARG} MB/s as minimum speed";
            min_speed=${OPTARG};;
        h)
            echo "Usage:  -v <ip_version_4or6>";
            echo "Option: -s <min_speed> MB/s -r <record_name> -k <NS1_API_KEY>";
            exit 1;;
    esac
done



if [ -z $ip_version ]; then 
    echo "NO IP VERSION received, CHECK -v <ip_version> as input!"; exit 1;
elif [ $ip_version -eq 4 ]; then
    #---------------------ipv4---------------------
    ./CloudflareST -url $url -dn 5 -sl $min_speed -tl $max_relay -f ./ip.txt -o ./result.csv

    v4spd=$(cat ./result.csv | head -n 2 | tail -n 1 | awk -F ',' '{print$6}' | awk -F '.' '{print$1}');
    v4addr=$(cat ./result.csv | head -n 2 | tail -n 1 | awk -F ',' '{print$1}');
    echo "Update CF selected v4ip as: $v4addr, speed at $v4spd"

    if [[ -n $rn ]]; then record_name=$rn; fi
    zone_name=$(echo $record_name | awk -F '.' '{print$(NF-1) "." $NF}')
    curl -k -X POST -H "X-NSONE-Key: $key" https://api.nsone.net/v1/zones/$zone_name/$record_name/A -d '{"zone": "'$zone_name'","domain": "'$record_name'","type": "A","answers": [{"answer": ["'$v4addr'"]}] }'

elif [ $ip_version -eq 6 ]; then
    #---------------------ipv6---------------------
    ./CloudflareST -url $url -dn 5 -sl $min_speed -tl $max_relay -f ./ipv6.txt -o ./resultv6.csv

    v6spd=$(cat ./resultv6.csv | head -n 2 | tail -n 1 | awk -F ',' '{print$6}' | awk -F '.' '{print$1}')
    v6addr=$(cat ./resultv6.csv | head -n 2 | tail -n 1 | awk -F ',' '{print$1}')
    echo "Update CF selected v6ip as: $v6addr, speed at $v6spd"

    if [[ -n $rn ]]; then record6_name=$rn; fi
    zone6_name=$(echo $record6_name | awk -F '.' '{print$(NF-1) "." $NF}')
    curl -k -X POST -H "X-NSONE-Key: $key" https://api.nsone.net/v1/zones/$zone6_name/$record6_name/AAAA -d '{"zone": "'$zone6_name'","domain": "'$record6_name'","type": "AAAA","answers": [{"answer": ["'$v6addr'"]}] }'

else
    echo "Input wrong ip version, should be 4 or 6!"; exit 1;
fi
