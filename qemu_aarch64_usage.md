```
wget -O debian12_aarch64.qcow2 https://cloud.debian.org/images/cloud/bookworm/latest/debian-12-nocloud-arm64.qcow2

# -smp label may not work on termux, use --accel as alternative
qemu-system-aarch64 -machine virt -m 2048 -smp 4 --accel tcg,thread=multi,tb-size=64 -cpu cortex-a76 \
-drive file=$PREFIX/share/qemu/edk2-aarch64-code.fd,if=pflash,format=raw,read-only=on \
-drive file=debian12_aarch64.qcow2,if=none,id=drive0,cache=writeback -device virtio-blk,drive=drive0,bootindex=0 \
-netdev user,id=vnet,hostfwd=:127.0.0.1:0-:22 \
-device virtio-net,netdev=vnet \
-nographic

```