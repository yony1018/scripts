#!/bin/sh
kill -9 "`pidof v2rayc_watchdog`"

pid=$(ps | awk '/[v]2ray -c \/etc\/storage\/v2ray_client\/v2rayc_config.json/{print $1}')

if [ "$pid" == "" ]; then
    echo "[v2rayClient not running]"
else
    echo "[v2rayClient stop]"
    kill $pid
fi