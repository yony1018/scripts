#!/bin/sh
cd /etc/storage/v2ray_client
sh v2rayc_stop.sh
sleep 1
./v2rayc_watchdog >/dev/null 2>&1 &
echo "v2rayc_watchdog started."

echo ""
echo "[v2rayClient restart]"
/usr/bin/v2ray -c /etc/storage/v2ray_client/v2rayc_config.json >/dev/null 2>&1 &