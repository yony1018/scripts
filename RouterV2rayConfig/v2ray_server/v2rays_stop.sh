#!/bin/sh
kill -9 "`pidof v2rays_watchdog`"

pid=$(ps | awk '/[v]2ray -c \/etc\/storage\/v2ray_server\/v2rays_config.json/{print $1}')

if [ "$pid" == "" ]; then
    echo "[v2rayServer not running]"
else
    echo "[v2rayServer stop]"
    kill $pid
fi