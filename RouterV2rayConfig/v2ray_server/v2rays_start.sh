#!/bin/sh
cd /etc/storage/v2ray_server
sh v2rays_stop.sh
sleep 1
./v2rays_watchdog >/dev/null 2>&1 &
echo "v2rays_watchdog started."

echo ""
echo "[v2rayServer restart]"
/usr/bin/v2ray -c /etc/storage/v2ray_server/v2rays_config.json >/dev/null 2>&1 &