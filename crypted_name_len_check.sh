#!/bin/bash

max_path_len=255
# $1 参数代表查找的根路径
search_root_path="$1"
# $2 参数用于计算相应需要减去的父路径加密长度
parent_path="$2"

crypted_parent_path_len=$(rclone cryptdecode --reverse crypt: "$parent_path" | wc -m)
raw_parent_path_len=$(echo "$parent_path" | wc -m)

# echo $crypted_parent_path_len
# echo $raw_parent_path_len
max_path_len=`expr $max_path_len + $crypted_parent_path_len - $raw_parent_path_len`
# echo $max_path_len

find $search_root_path -type f -print0 | xargs -0 -n 1 rclone cryptdecode --reverse crypt: | awk '{if (length($2) > '$max_path_len') print $1}'