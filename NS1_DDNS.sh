#!/bin/bash

key="fu8vUVOWETQmTncvTowU"
pub_ip=$(curl -4 ifconfig.co/ip)
v6addr=$(ip addr | grep "inet6" | grep "global dynamic" | awk -F '/' '{print$1}' | awk '{print$2}')
v4addr=$(ip addr | grep "inet" | grep "global" | grep "ppp0" | awk -F '/' '{print$1}' | awk '{print$2}')
zone_name=""
record_name=""
record_type=""
ipaddr=""
method="POST"

if [ "$pub_ip" == "$v4addr" ]; then
    echo "Public ip is EQUAL to local ip! That's OK!"
    else
    echo "Public ip is NOT EQUAL to local ip! Use public ip as default!"
    v4addr=$pub_ip
fi

while getopts ":k:r:t:i:m:h:" flag
do
    case "${flag}" in
        k)  
            echo "Using NSONE_API_KEY: ${OPTARG}";
            key=${OPTARG};;
        r) 
            echo "Using record name: ${OPTARG}";
            record_name=${OPTARG};;
        t)
            echo "Using record type: <${OPTARG}>";
            record_type=${OPTARG};;
        i)
            echo "Using ip addr: ${OPTARG}";
            ipaddr=${OPTARG};;
        m)
            echo "Using http method: ${OPTARG}";
            method=${OPTARG};;
        h)
            echo "Usage:  -r <record_name> -t <record_type> -i <ip_addr>";
            echo "Option: -k <api_key> -m <PUT_method_for_addnew>";
            exit 1;;
    esac
done


if [ -z $key ]
then echo "NO API_KEY received, CHECK -k <api_key> as input!"; 
exit 1;
fi

if [ -z $record_name ]
then echo "NO RECORD NAME received, CHECK -r <record_name> as input!"; 
exit 1;
fi

if [ -z $record_type ]
then echo "NO RECORD TYPE received, CHECK -t <record_type> as input!"; 
exit 1;
fi

if [ -z $ipaddr ]
then 
    echo "NO IP ADDRESS received, CHECK -i <ip_addr> as input! Update ip with AUTO-GRUBED NOW!";
    if [ "$record_type" == "AAAA" ]
    then 
        ipaddr=$v6addr;
        echo "update ipv6 addr: $ipaddr";
    elif [ "$record_type" == "A" ]
    then 
        ipaddr=$v4addr;
        echo "update ipv4 addr: $ipaddr";
    else echo "Unknown ip type!";
    fi
    if [ -z $ipaddr ]
    then echo "IP ADDRESS INPUT STILL EMPTY! EXIT!"
    exit 1;
    fi
fi

zone_name=$(echo $record_name | awk -F '.' '{print$(NF-1) "." $NF}')
curl -k -X $method -H "X-NSONE-Key: $key" https://api.nsone.net/v1/zones/$zone_name/$record_name/$record_type -d '{"zone": "'$zone_name'","domain": "'$record_name'","type": "'$record_type'","answers": [{"answer": ["'$ipaddr'"]}] }'
