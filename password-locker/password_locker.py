import sys

def letter_shift(letter,offset):
  if ord(letter) <= ord('z') and ord(letter) >= ord('a'):
    return chr((ord(letter) - ord('a') + offset) % 26 + ord('a'))
  elif  ord(letter) <= ord('9') and ord(letter) >= ord('0'):
    return chr(int(letter)+offset+ord('a'))
  else:
    raise Exception('The FIRST 6 DIGITS of the input must be PURE numbers or letters')

def letter_trans(initpw):
  keys=[2,6,1,0,1,8]
  initpw=initpw*6
  output='A'
  for idx in range(6):
      output+=letter_shift(initpw[idx],keys[idx])
  return output

def number_trans(level1_pw):
  output =chr((ord(level1_pw[1]) - ord('a') + 1) % 10 + ord('0'))
  output+=chr((ord(level1_pw[2]) - ord('a') + 1) % 10 + ord('0'))
  output+=chr((ord(level1_pw[3]) - ord('a') + 1) % 10 + ord('0'))
  return output

def number_trans_pure_6_numbers(level1_pw):
  output =chr((ord(level1_pw[1]) - ord('a') + 1) % 10 + ord('0'))
  for i in range(2,7):
    output+=chr((ord(level1_pw[i]) - ord('a') + 1) % 10 + ord('0'))
  return output

def pw_locker_api(input):
  letters=letter_trans(input)
  numbers=number_trans(letters)
  print('Return a list as [<fulltext_encryption>,<6pin>]')
  return numbers+ '@' + letters,number_trans_pure_6_numbers(letters)

if __name__ == "__main__":
    ## 3numbers - @ - A - 6letters
    # 变换加密：
    # 输入任意长度数字和字母混合字符串：如1daa23a
    # 将字符串重复六次提取前六位
    # 对每一位字母按照该位key值向右进行位移
    # 对每一位数字，其本身加上该位key值作为位移量，对a进行位移
    # 得到一串6位纯字母字符串6letters
    # 对其前三位字母在字母表上的位置值对10同余（a为1，z为6）
    # 得到一串3位纯数字字符串3numbers
    # 组合得到密码为3numbers-@A-6letters
    
    print('Input init string is: ' + sys.argv[1])
    print('Encrypt as 3numbers - @ - A - 6letters')
    letters=letter_trans(sys.argv[1])
    numbers=number_trans(letters)
    print(numbers+ '@' + letters)
    print(number_trans_pure_6_numbers(letters))


