/**
 * Welcome to Cloudflare Workers! This is your first worker.
 *
 * - Run "npm run dev" in your terminal to start a development server
 * - Open a browser tab at http://localhost:8787/ to see your worker in action
 * - Run "npm run deploy" to publish your worker
 *
 * Learn more at https://developers.cloudflare.com/workers/
 */

export default {
  async fetch(request, env, ctx) {
    const url = new URL(request.url);
    const input = url.pathname.substring(1);
    // return new Response(input); 
    return new Response(number_trans(input) + "@" + letter_trans(input) + '\n' + number_trans_pure_6_numbers(letter_trans(input)));
  },
};

function letter_shift(letter,offset) {
  if (letter.length != 1)
    throw "wrong lenth!";
  if (letter <= 'z' && letter >= 'a')
    return String.fromCharCode((letter.charCodeAt() - 'a'.charCodeAt() + offset) % 26 + 'a'.charCodeAt());
  else if (letter <= '9' && letter >= '0')
    return String.fromCharCode(Number(letter) + offset + 'a'.charCodeAt());
  else
    throw "The FIRST 6 DIGITS of the input must be PURE numbers or letters";
}

function letter_trans(initpw) {
  var keys = [2,6,1,0,1,8];
  initpw = initpw + initpw + initpw;
  initpw = initpw + initpw;
  var output = 'A';
  for (var idx=0;idx<6;idx++)
    output += letter_shift(initpw[idx],keys[idx]);
  return output;
}

function number_trans(level1_pw) {
  var output = String.fromCharCode((level1_pw[1].charCodeAt() - 'a'.charCodeAt() + 1 ) % 10 + '0'.charCodeAt());
  output += String.fromCharCode((level1_pw[2].charCodeAt() - 'a'.charCodeAt() + 1 ) % 10 + '0'.charCodeAt());
  output += String.fromCharCode((level1_pw[3].charCodeAt() - 'a'.charCodeAt() + 1 ) % 10 + '0'.charCodeAt());
  return output;
}

function number_trans_pure_6_numbers (level1_pw) {
  var output = "";
  for (var i=1;i<7;i++)
    output += String.fromCharCode((level1_pw[i].charCodeAt() - 'a'.charCodeAt() + 1 ) % 10 + '0'.charCodeAt());
  return output;
}

