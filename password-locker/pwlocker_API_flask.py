from flask import Flask
from password_locker import pw_locker_api
app = Flask(__name__)


@app.route('/')
def hello():
    return 'Hello! This API is for some PERSONAL usage.'

@app.route('/<init_password>')
def get_fulltext(init_password):
    return pw_locker_api(init_password)[0]

@app.route('/6/<init_password>')
def get_6pin(init_password):
    return pw_locker_api(init_password)[1]

if __name__ == '__main__':
    app.run(host='0.0.0.0',port=2082)
    #API for ipv6:
    #app.run(host='::',port=2082)