## Setup onfigure from hub

`https://gitlab.com/yony1018/scripts/-/raw/main/ClashConfig/config.yaml`

## Download clients:
- Linux-Server-X86 non-display-client

`wget -e "https_proxy=http://www.yonyvp.cf:10080" https://release.dreamacro.workers.dev/latest/clash-linux-amd64-latest.gz`

- Ubuntu GUI Client

`wget -e "https_proxy=http://www.yonyvp.cf:10080" https://github.com/Fndroid/clash_for_windows_pkg/releases/download/0.20.37/Clash.for.Windows-0.20.37-x64-linux.tar.gz`

- Windows Setup Client

`https://github.com/Fndroid/clash_for_windows_pkg/releases/download/0.20.37/Clash.for.Windows.Setup.0.20.37.exe`

- Windows Portable Client

`https://github.com/Fndroid/clash_for_windows_pkg/releases/download/0.20.37/Clash.for.Windows-0.20.37-win.7z`

- Android Client

`https://github.com/Kr328/ClashForAndroid/releases/download/v2.5.12/cfa-2.5.12-premium-universal-release.apk`
