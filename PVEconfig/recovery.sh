#!/bin/bash
pkg_name=$1

#解包
cd /root/
tar -xvf /root/$pkg_name.tar -C /root/
#备份原文件
bash /root/$pkg_name/backup.sh

#静默配置恢复：
#######TODO: 节点pve->凭证：手动更新证书
#...
#vm虚拟机配置
cp -r /root/$pkg_name/pve/nodes/pve/qemu-server/ /etc/pve/nodes/pve/
cp /root/$pkg_name/root /var/spool/cron/crontabs/root
cp /root/$pkg_name/NS1_DDNS.sh /root/NS1_DDNS.sh
#外部系统监控面板配置
cp /root/$pkg_name/status.cfg /etc/pve/status.cfg
#数据中心备注、键盘布局（US）
cp /root/$pkg_name/datacenter.cfg /etc/pve/datacenter.cfg
#节点pve备注
cp /root/$pkg_name/config /etc/pve/local/config
#备份任务配置
cp /root/$pkg_name/jobs.cfg /etc/pve/jobs.cfg

#动态配置恢复
#换源：
cp /root/$pkg_name/sources.list /etc/apt/sources.list 
#禁用pve企业源
cp -r /root/$pkg_name/sources.list.d/ /etc/apt/ 
apt update

#######TODO: (覆盖前确认ssd盘是否是/dev/sdc，建立/mnt/ssd1目录，并用mount -a后df -h检验！)#######
#cp /root/$pkg_name/fstab /etc/fstab
#echo UUID=650d0b6e-5941-40fd-9ad5-45aab1840050 /mnt/nvme1 xfs nofail,x-systemd.device-timeout=3s    0  2 >> /etc/fstab
echo UUID=700f9a49-e075-405e-9490-44cbdf65b705 /mnt/nvme2 xfs nofail,x-systemd.device-timeout=3s    0  2 >> /etc/fstab
mount -a

#######TODO: 删除local-lvm 扩容/temp
lvremove pve/data
lvextend -l +100%FREE -r pve/root

#配置pve存储目录
cp /root/$pkg_name/storage.cfg /etc/pve/storage.cfg

#ipv6设置
cp /root/$pkg_name/sysctl.conf /etc/sysctl.conf 
# net.ipv6.conf.all.accept_ra=2 >> /etc/sysctl.conf 
# net.ipv6.conf.default.accept_ra=2 >> /etc/sysctl.conf 
# net.ipv6.conf.vmbr0.accept_ra=2 >> /etc/sysctl.conf 
# net.ipv6.conf.all.autoconf=1 >> /etc/sysctl.conf 
# net.ipv6.conf.default.autoconf=1 >> /etc/sysctl.conf 
# net.ipv6.conf.vmbr0.autoconf=1 >> /etc/sysctl.conf 
sysctl -p

#CT模板换源：
#cp /root/$pkg_name/APLInfo.pm /usr/share/perl5/PVE/APLInfo.pm
sed -i 's/http:\/\/download.proxmox.com\/images/https:\/\/mirrors.tuna.tsinghua.edu.cn\/proxmox\/images/g' /usr/share/perl5/PVE/APLInfo.pm
#重启CT服务
systemctl restart pvedaemon.service


#配置UPS：
apt update
apt install apcupsd -y
cp /root/$pkg_name/apcupsd.conf /etc/apcupsd/apcupsd.conf
systemctl restart apcupsd
systemctl status apcupsd
systemctl enable apcupsd

#前端界面温度监测+取消登录订阅提示：
apt-get update
apt-get install lm-sensors -y
sensors-detect
sensors
# cp /root/$pkg_name/Nodes.pm /usr/share/perl5/PVE/API2/Nodes.pm
# cp /root/$pkg_name/pvemanagerlib.js /usr/share/pve-manager/js/pvemanagerlib.js
# cp /root/$pkg_name/proxmoxlib.js /usr/share/javascript/proxmox-widget-toolkit/proxmoxlib.js

#######/usr/share/perl5/PVE/API2/Nodes.pm   :
#此行后添加     PVE::pvecfg::version_text();
	$res->{TemperatureInfo} = `sensors asus_wmi_sensors-virtual-0`;
	$res->{VoltageInfo} = `sensors asus_wmi_sensors-virtual-0`;
	$res->{cpuFansInfo} = `sensors asus_wmi_sensors-virtual-0`;
	$res->{sysFansInfo} = `sensors asus_wmi_sensors-virtual-0`;
	$res->{NVME1Info} = `sensors nvme-pci-0100`;
	$res->{NVME2Info} = `sensors nvme-pci-0800`;
	$res->{UPSInfo} = `apcaccess`;

#######/usr/share/pve-manager/js/pvemanagerlib.js   :
#定位到此框后添加     renderer: Proxmox.Utils.render_cpu_model,
      {
			itemId: 'TemperatureInfo',
			colspan: 2,
			printBar: false,
			title: gettext('System Temperatures:'),
			textField: 'TemperatureInfo',
			renderer:function(value){
				const tCPU = value.match(/CPU Temperature.*?\+([\d\.]+)?/)[1];
				const tMB = value.match(/Motherboard Temperature.*?\+([\d\.]+)?/)[1];
				const tCS = value.match(/Chipset Temperature.*?\+([\d\.]+)?/)[1];
				const tOPT = value.match(/Tsensor 1 Temperature.*?\+([\d\.]+)?/)[1];
				return `CPU : ${tCPU}C | MotherBoard : ${tMB}C | Chipset : ${tCS}C | Tsensor : ${tOPT}C`
			}
		},
		{
			itemId: 'NVME1Info',
			colspan: 2,
			printBar: false,
			title: gettext('NVME Temperatures:'),
			textField: 'NVME1Info',
			renderer:function(value){
				const c1 = value.match(/Composite.*?\+([\d\.]+)?/)[1];
				return `                                NVME 1 : ${c1}C  `
			}
		},
		{
			itemId: 'NVME2Info',
			colspan: 2,
			printBar: false,
			title: gettext(' '),
			textField: 'NVME2Info',
			renderer:function(value){
				const c1 = value.match(/Composite.*?\+([\d\.]+)?/)[1];
				return `                                NVME 2 : ${c1}C  `
			}
		},
		{
			itemId: 'UPSInfo',
			colspan: 2,
			printBar: false,
			title: gettext('UPS Information:'),
			textField: 'UPSInfo',
			renderer:function(value){
				const c0 = value.match(/LOADPCT.*[0-9]/);
				const c1 = value.match(/BCHARGE.*[0-9]/);
				const c2 = value.match(/TIMELEFT.*[0-9]/);
				return `${c0}% | ${c1}% |  ${c2}minutes`
			}
		},
		{
			itemId: 'cpuFansInfo',
			colspan: 2,
			printBar: false,
			title: gettext('System Fans:'),
			textField: 'cpuFansInfo',
			renderer:function(value){
				const fCPU = value.match(/CPU Fan.*[0-9]/);
				const fCPUopt = value.match(/CPU OPT.*[0-9]/);
				const fAIO = value.match(/AIO Pump.*[0-9]/);
				const fWaterP = value.match(/Water Pump.*[0-9]/);
				return `${fCPU}RPM | ${fCPUopt}RPM | ${fAIO}RPM | ${fWaterP}RPM`
			}
		},
		{
			itemId: 'sysFansInfo',
			colspan: 2,
			printBar: false,
			title: gettext(' '),
			textField: 'sysFansInfo',
			renderer:function(value){
				const f1 = value.match(/Chassis Fan 1.*[0-9]/);
				const f2 = value.match(/Chassis Fan 2.*[0-9]/);
				const f3 = value.match(/Chassis Fan 3.*[0-9]/);
				return `${f1}RPM | ${f2}RPM | ${f3}RPM`
			}
		},
		{
			itemId: 'VoltageInfo',
			colspan: 2,
			printBar: false,
			title: gettext('System Voltage:'),
			textField: 'VoltageInfo',
			renderer:function(value){
				const vCPU = value.match(/CPU Core Voltage.*[0-9]/);
				const v12 = value.match(/\+12V Voltage.*[0-9]/);
				const v5 = value.match(/\+5V Voltage.*[0-9]/);
				const v3 = value.match(/3VSB Voltage.*[0-9]/);
				return `${vCPU}mV | ${v12}V | ${v5}V | ${v3}V`
			}
		},


#定位到框架中   widget.pveNodeStatus
#height 300 改为 500


#######/usr/share/javascript/proxmox-widget-toolkit/proxmoxlib.js
#定位到框架中： No valid subscription
#   Ext.Msg.show     替换为  void



systemctl restart pveproxy


#配置虚拟化参数！！！重要！！！
#cp /root/$pkg_name/grub /etc/default/grub
sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT=\"quiet\"/GRUB_CMDLINE_LINUX_DEFAULT=\"quiet iommu=pt amd_iommu=on drm.debug=0 kvm_amd.nested=1 kvm.ignore_msrs=1 kvm.report_ignored_msrs=0 video=efifb:off,vesafb:off pcie_acs_override=downstream,multifunction pci=assign-busses vfio_iommu_type1.allow_unsafe_interrupts=1\"/g' /etc/default/grub
update-grub

cp /root/$pkg_name/modules /etc/modules
cp -r /root/$pkg_name/modprobe.d/ /etc/
update-initramfs -u


#######TODO: reboot needed

####---------------reboot back-----------------####

#######TODO: 安装linux6.1 kernel
#apt update
#apt install pve-kernel-6.1
#######TODO: reboot needed

####---------------reboot back-----------------####
