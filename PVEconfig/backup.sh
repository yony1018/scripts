#!/bin/bash
datename=backup_pvecfg_$(date +%Y%m%d-%H%M%S)

mkdir /root/$datename/
cp /etc/default/grub /root/$datename/
cp /etc/modules /root/$datename/
cp -r /etc/modprobe.d/ /root/$datename/modprobe.d/
cp /etc/fstab /root/$datename/
cp /etc/apt/sources.list /root/$datename/
cp -r /etc/apt/sources.list.d/ /root/$datename/sources.list.d/
cp /etc/sysctl.conf /root/$datename/
cp /var/spool/cron/crontabs/root /root/$datename/
cp /root/NS1_DDNS.sh /root/$datename/
cp /root/backup.sh /root/$datename/
cp /usr/share/perl5/PVE/APLInfo.pm /root/$datename/
cp /usr/share/perl5/PVE/API2/Nodes.pm /root/$datename/
cp /usr/share/pve-manager/js/pvemanagerlib.js /root/$datename/
cp /usr/share/javascript/proxmox-widget-toolkit/proxmoxlib.js /root/$datename/
cp /etc/apcupsd/apcupsd.conf /root/$datename/
###update 230927
cp /etc/pve/storage.cfg /root/$datename/
cp /etc/pve/jobs.cfg /root/$datename/
cp /etc/pve/status.cfg /root/$datename/
cp /etc/pve/datacenter.cfg /root/$datename/
cp /etc/pve/local/config /root/$datename/

cp -r /etc/pve/ /root/$datename/pve/

cd /root/
tar -cvf /root/$datename.tar $datename --remove-files
cp /root/$datename.tar /var/lib/vz/dump/
cp /root/$datename.tar /mnt/nvme2/dump/
mv /root/$datename.tar /mnt/pve/NFSdump/dump/
