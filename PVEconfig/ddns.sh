#!/bin/bash

v6addr=$(ip addr | grep "inet6" | grep "dynamic" | grep "24" | awk -F '/' '{print$1}' | awk '{print$2}')
v4addr=$(ip addr | grep "inet" | grep "global" | grep "ppp0" | awk -F '/' '{print$1}' | awk '{print$2}')

echo "update ddns v6 as: $v6addr"
echo "update ddns v4 as: $v4addr"

echo "hostname: $1"

echo "alias: $2"

echo "proxy type: $3"

echo "proxy addr: $4"

echo "proxy port: $5"

curl --proxy "$3://yony:261018@$4:$5" "http://api.dynu.com/nic/update?hostname=$1&alias=$2&username=yony&password=261018&myipv6="$v6addr""