## transfer server configure scripts

### Prepare Initialization
- Connectivity
```
sudo -i
passwd
    {mypassword}
apt update
apt install openssh-server
sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config
sed -i 's/#PasswordAuthentication yes/PasswordAuthentication yes/g' /etc/ssh/sshd_config
service sshd restart

#建议手动
alias={cliname}
hostname=ctf.giize.com
curl -4 "http://api.dynu.com/nic/update?hostname=$hostname&alias=$alias&username=yony&password=261018" &
```

- Firewall

```
sudo apt-get install ufw
sudo ufw enable
```

- Cert

``` 
#安装acme：
curl https://get.acme.sh | sh
#添加软链接
ln -s  /root/.acme.sh/acme.sh /usr/local/bin/acme.sh
#切换CA机构： 
acme.sh --set-default-ca --server letsencrypt
#申请证书： 
acme.sh  --issue -d $alias.tsf.giize.com -k ec-256 --webroot  /var/www/html
#安装证书：
acme.sh --install-cert -d 你的域名 --ecc --key-file /etc/x-ui/server.key  --fullchain-file /etc/x-ui/server.crt --reloadcmd "systemctl force-reload nginx"
```

- X-ui

```
bash <(curl -Ls https://raw.githubusercontent.com/vaxilu/x-ui/master/install.sh)

配置uuid：
1018aaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa

apt install sshpass
alias={}
sshpass -p '261018' rsync -a --delete root@yony.giize.com:/www/server/panel/vhost/letsencrypt/$alias.cft.giize.com/ /root/cert/
```

