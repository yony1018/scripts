## Server Configuation

### Proxy Server config

> - ```
>   nano /etc/hostname
>   {proxyserver}
>   ```
>
> - ```
>   sudo -i
>   mkdir /root/clash
>   cd /root/clash
>   wget -O clash.gz https://gitlab.com/yony1018/scripts/-/raw/main/ClashConfig/clash-linux-amd64-2023.03.04.gz
>   gzip -f clash.gz -d
>   chmod 777 clash
>   wget https://gitlab.com/yony1018/scripts/-/raw/main/ClashConfig/config.yaml
>   nano /etc/systemd/system/clash.service
>   ```
>

```
[Unit]
# DESCRIPTION for clash
Description = clash service
After = network.target syslog.target
Wants = network.target

[Service]
Type = simple
# start with clash
ExecStart = /root/clash/clash -d /root/clash
Restart = always
RestartSec = 5
StartLimitInterval = 0

[Install]
WantedBy = multi-user.target

```

>
> - ```
>   systemctl daemon-reload
>   service clash restart
>   service clash status
>   systemctl enable clash
>         
>   crontab -e
>   ```

```
#update clash rules
0 6 * * * wget -O /root/clash/config.yaml https://gitlab.com/yony1018/scripts/-/raw/main/ClashConfig/config.yaml > /dev/null 2>&1
5 6 * * * service clash restart > /dev/null 2>&1
```

> - ```
>   apt install socat curl
>   cd /root
>   nano socat.sh
>   ```
>

```
#!/bin/sh

proc=/usr/bin/socat
logfile="/dev/null"

echo "Strating socat processes..."

#WorkStation RDP
nohup $proc TCP6-LISTEN:11101,reuseaddr,fork TCP4:192.168.1.101:3333 >> $logfile 2>&1 &
#WorkStation-M40 RDP
nohup $proc TCP6-LISTEN:11103,reuseaddr,fork TCP4:192.168.1.103:3333 >> $logfile 2>&1 &
#PlayStation RDP
nohup $proc TCP6-LISTEN:11131,reuseaddr,fork TCP4:192.168.1.131:3333 >> $logfile 2>&1 &
#PlayStation-AndroidADB RDP
nohup $proc TCP6-LISTEN:5555,reuseaddr,fork TCP4:192.168.1.130:5555 >> $logfile 2>&1 &
#Watch dog
nohup $proc TCP6-LISTEN:3000,reuseaddr,fork TCP4:192.168.1.204:3000 >> $logfile 2>&1 &
#Sunshine&Moonlight
nohup $proc TCP6-LISTEN:47984,reuseaddr,fork TCP4:192.168.1.131:47984 >> $logfile 2>&1 &
nohup $proc TCP6-LISTEN:47989,reuseaddr,fork TCP4:192.168.1.131:47989 >> $logfile 2>&1 &
nohup $proc TCP6-LISTEN:48010,reuseaddr,fork TCP4:192.168.1.131:48010 >> $logfile 2>&1 &
nohup $proc UDP6-LISTEN:5353,reuseaddr,fork UDP4:192.168.1.131:5353 >> $logfile 2>&1 &
nohup $proc UDP6-LISTEN:47998,reuseaddr,fork UDP4:192.168.1.131:47998 >> $logfile 2>&1 &
nohup $proc UDP6-LISTEN:47999,reuseaddr,fork UDP4:192.168.1.131:47999 >> $logfile 2>&1 &
nohup $proc UDP6-LISTEN:48002,reuseaddr,fork UDP4:192.168.1.131:48002 >> $logfile 2>&1 &
nohup $proc UDP6-LISTEN:48010,reuseaddr,fork UDP4:192.168.1.131:48010 >> $logfile 2>&1 &
#temp RDP
nohup $proc TCP6-LISTEN:1187,reuseaddr,fork TCP4:192.168.1.87:3333 >> $logfile 2>&1 &

echo "Completed. "
exit 0

```
>
> - ```
>   chmod 777 socat.sh
>   nano /etc/systemd/system/socat.service
>   ```
```
[Unit]
Description=Socat Multi-forward Server Service
Documentation=man:socat
After=network.target

[Service]
Type=simple
User=root
ExecStart=/root/socat.sh
ExecStop=/usr/bin/pkill socat
RemainAfterExit=yes

[Install]
WantedBy=multi-user.target
```
> - ```
>   systemctl daemon-reload
>   service socat restart
>   service socat status
>   systemctl enable socat
>   ```
> - ```
>   bash <(curl -Ls https://raw.githubusercontent.com/vaxilu/x-ui/master/install.sh)
>   ```

### General Settings

> - ```
>   sudo -i
>   passwd
>   {mypassword}
>   sudo apt update
>   apt install openssh-server
>   apt install qemu-guest-agent
>   sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config
>   sed -i 's/#PasswordAuthentication yes/PasswordAuthentication yes/g' /etc/ssh/sshd_config
>   service sshd restart
>       
>   sudo sed -i "s@http://.*archive.ubuntu.com@https://mirrors.tuna.tsinghua.edu.cn@g" /etc/apt/sources.list
>       
>   cd /root
>   nano ddns.sh
>       
>   ```
>
> - ```
>   #!/bin/bash
>       
>   v6addr=$(ip addr | grep "inet6" | grep "global dynamic" | awk -F '/' '{print$1}' | awk '{print$2}')
>       
>   echo "update ddns as: $v6addr"
>       
>   echo "hostname: $1"
>       
>   echo "alias: $2"
>       
>   echo "proxy type: $3"
>       
>   echo "proxy addr: $4"
>       
>   echo "proxy port: $5"
>       
>   curl --proxy "$3://yony:261018@$4:$5" "http://api.dynu.com/nic/update?hostname=$1&alias=$2&username=yony&password=261018&myipv6="$v6addr""
>       
>   ```
>
>   ```
> 
> - ```
>   chmod 777 /root/ddns.sh
>   crontab -e
>     
>   # */1 * * * * /root/ddns.sh {hostname} {alias} {proxy_type} {proxy_addr} {proxy_port} > /dev/null 2>&1
> 
>   */1 * * * * /root/ddns.sh {hostname} {alias} http 192.168.1.1 7890 > /dev/null 2>&1
>   */5 * * * * /root/ddns.sh {hostname} {alias} http 192.168.1.201 7890 > /dev/null 2>&1
>   */10 * * * * /root/ddns.sh {hostname} {alias} socks5h yony.giize.com 10081 > /dev/null 2>&1

>   ```

