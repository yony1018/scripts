#!/usr/bin/env python3

import os
import sys

def md_generator(start, pattarn):
    if not os.path.exists('annotation_index/'):
        os.makedirs('annotation_index/')
    for relpath, dirs, files in os.walk(start):
        for file in files:
            if file.endswith(pattarn):
                full_path = os.path.join(start, relpath, file)
                full_path = os.path.normpath(full_path)
                md_full_path='annotation_index/' + file[:-(len(pattarn))] + 'md'
                if os.path.isfile(md_full_path):
                    continue
                f = open(md_full_path,'w')
                f.write('---\n')
                f.write('annotation-target: ' + full_path + '\n')
                f.write('---')
                f.close()
                print('Generate md file at: ' + md_full_path)


if __name__ == '__main__':
    workpath = os.path.dirname(sys.argv[0])
    os.chdir(workpath)
    if len(sys.argv)==1:
        #默认对当前py脚本所在目录下pdf生成markdown文件
        md_generator('./','pdf')
    elif len(sys.argv)==3:
        md_generator(sys.argv[1], sys.argv[2])

exit