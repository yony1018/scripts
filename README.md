# Scripts
> testing Github Desk sync

## DUCK DDNS config
```
cd /root
wget https://gitlab.com/yony1018/scripts/-/raw/main/DUCK_DDNS.sh
chmod 777 ./DUCK_DDNS.sh
```

`crontab -e`

```
*/5 * * * * /root/DUCK_DDNS.sh -r <domain_name> -t <A_or_AAAA> > /dev/null 2>&1
```

## NS1 DDNS config
```
cd /root
wget https://gitlab.com/yony1018/scripts/-/raw/main/NS1_DDNS.sh
chmod 777 ./NS1_DDNS.sh
```

`crontab -e`

```
*/5 * * * * /root/NS1_DDNS.sh -r <domain_name> -t <A_or_AAAA> > /dev/null 2>&1
```

## crypted_name_len_check.sh USAGE
- Setup rclone crypt correctly
- Check details in scripts
