#!/bin/bash

# wget -O /dev/null "https://www.duckdns.org/update?domains=yonyserver&token=5fc5f934-0138-485e-9372-d27d1c6b0774&ip=$v4addr&ipv6=$v6addr"  &

token="5fc5f934-0138-485e-9372-d27d1c6b0774"


hosts=("checkip.amazonaws.com" "api.ipify.org" "ifconfig.me/ip" "icanhazip.com" "ipinfo.io/ip" "ipecho.net/plain" "checkipv4.dedyn.io")
CURL=`which curl`
DIG=`which dig`
# 没必要用，有时还会报错
# check=$($DIG +short myip.opendns.com @resolver1.opendns.com A)  
if [ ! $? -eq 0 ] || [ -z "$check" ] || [[ ! $check =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
    echo "Unable to get your public IP address by OpenDNS service, try to another way."
    count=${#hosts[@]}
    while [ -z "$check" ] && [[ $count -ne 0 ]]; do
        selectedhost=${hosts[ $RANDOM % ${#hosts[@]} ]}
        check=$($CURL -4s https://$selectedhost | grep '[^[:blank:]]') && {
            if [ -n "$check" ] && [[ $check =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
                break
            else
                check=""
                count=$(expr $count - 1)
                echo "The host $selectedhost returned an invalid IP address."
            fi
        } || {
            check=""
            count=$(expr $count - 1)
            echo "The host $selectedhost did not respond."
        }
    done
fi
if [ -z "$check" ]; then
    echo "Unable to get your public IP address. Please check your internet connection."
fi
echo "Your public IP address is $check"


v6addr=$(ip addr | grep "inet6" | grep "global dynamic" | awk -F '/' '{print$1}' | awk '{print$2}')
v4addr=$(ip addr | grep "inet" | grep "global" | grep "ppp0" | awk -F '/' '{print$1}' | awk '{print$2}')

if [ "$check" == "$v4addr" ]; then
    echo "Public ip is EQUAL to local ip! That's OK!"
    else
    echo "Public ip is NOT EQUAL to local ip! Use public ip as default!"
    v4addr=$check
fi



while getopts ":b:r:t:i:l:p:h:" flag
do
    case "${flag}" in
        r) 
            echo "Using record name: ${OPTARG}";
            record_name=${OPTARG};;
        t)
            echo "Using record type: <${OPTARG}>";
            record_type=${OPTARG};;
        i)
            echo "Using ip addr: ${OPTARG}";
            ipaddr=${OPTARG};;
        h)
            echo "Usage: -r <record_name> -t <record_type> -i <ip_addr>";
            exit 1;;
    esac
done

alias=$(echo $record_name | awk -F '.' '{print$1}')


if [ -z $record_name ]
then echo "NO RECORD NAME received, CHECK -r <record_name> as input!"; 
exit 1;
fi

if [ -z $record_type ]
then echo "NO RECORD TYPE received, CHECK -t <record_type> as input!"; 
exit 1;
fi

if [ -z $ipaddr ]
then 
    echo "NO IP ADDRESS received, CHECK -i <ip_addr> as input! OR update ip with AUTO-GRUBED";
    if [ "$record_type" == "AAAA" ]
    then 
        ipaddr=$v6addr;
        echo "update ipv6 addr: $ipaddr";
        curl -k "https://www.duckdns.org/update?domains=$alias&token=$token&ipv6=$v6addr"
    elif [ "$record_type" == "A" ]
    then 
        ipaddr=$v4addr;
        echo "update ipv4 addr: $ipaddr";
        curl -k "https://www.duckdns.org/update?domains=$alias&token=$token&ip=$v4addr"
    else echo "Unknown ip type!";
    fi
    if [ -z $ipaddr ]
    then echo "IP ADDRESS INPUT STILL EMPTY! EXIT!"
    exit 1;
    fi
fi
